package com.pixeltron.heartbeat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.content.Context;



import com.unity3d.player.UnityPlayerActivity;

public class MainActivity extends UnityPlayerActivity {

    SensorManager mSensorManager;
    Sensor mSensor;
    static float myRate = 0.0f;
    static float exposedRate = 0.0f;
    static String logString = "";
    static boolean flushLog = false;

    public static Context mContext;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mContext = this;

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE);
        log("Activity created");
    }

    private final SensorEventListener mSensorListener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent event) {
            // TODO Auto-generated method stub

            myRate = event.values[0];
            log("On Sensor Changed " + Float.toString(myRate));

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // TODO Auto-generated method stub
            int i = 0;
        }

    };

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        log("Registering sensor listener");
        mSensorManager.registerListener(mSensorListener, mSensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE), SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onStop() {
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        log("Unregistering listener");
        mSensorManager.unregisterListener(mSensorListener);
        super.onPause();
    }

    int i = 0;

    public void log(String s)
    {
        flushLog = false;
        logString += s + "\n";
    }

    //repeated calls to getLog will result in "" being returned.
    //is only non empty if log calls have been made in the interim.
    public static String getLog()
    {
        if (flushLog)
        {
            logString = "";
        }
        flushLog = true;
        return logString;
    }

    //every time we get the rate we want to reset it to -1.
    public static float getRate()
    {
        exposedRate = myRate;
        myRate = -1;
        return exposedRate;
    }
}
