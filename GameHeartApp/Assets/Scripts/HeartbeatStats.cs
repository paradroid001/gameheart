﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;


[System.Serializable]
public class Stats
{
    //string name; //put date / time here?
    public float totalTime;
    public float maxRate;
    public float minRate;
    public float avgRate;
    public float[] heartRateDataPoints;

    public Stats(float t, float min, float max, float avg, float[] points)
    {
        totalTime = t;
        heartRateDataPoints = points;
        minRate = min;
        maxRate = max;
        avgRate = avg;
    }
}


public class HeartbeatStats
{
    const float lowerPulseThreshold = 1.0f;
    const float higherPulseThreshold = 20.0f;
    string name; //name of this sample
    float totalTime; //total time sampled
    //used to scale a screen full of values for amplitute purposes
    public float runningMin;     
    public float runningMax;
    //float heartrateCalculationPeriod = 0.5f; //every half a second
    List<float> heartRateDataPoints; //data we keep on heart rate
    List<float> _beatTimes;
    private int _numBeats;
    private float _prevBeatLevel; //the previous beat level.
    private float _highestHeartRate;
    private float _lowestHeartRate;
    float _runningAvg;
    
    public void Save()
    {
        if (totalTime < 120.0f)
            return;

        string datapath = Application.persistentDataPath + "/" + System.DateTime.UtcNow.ToString("dd_MM_yyyy_HH_mm") + ".json";
        Stats statsobject = new Stats(totalTime, _lowestHeartRate, _highestHeartRate, _runningAvg, heartRateDataPoints.ToArray());
        string json = JsonUtility.ToJson(statsobject);
        using (FileStream fs = new FileStream(datapath, FileMode.Create))
        {
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.Write(json);
                writer.Close();
                writer.Dispose();
            }
            fs.Close();
            fs.Dispose();
        }
    }


    public HeartbeatStats()
    {
        heartRateDataPoints = new List<float>();
        _numBeats = 0;
        _beatTimes = new List<float>();
        _prevBeatLevel = 0.0f;
        _highestHeartRate = 0.0f;
        _runningAvg = 0.0f;
        _lowestHeartRate = 100f;
    }

    //get the most recent heart rate.
    public float GetHeartRate()
    {
        int len = heartRateDataPoints.Count;
        if (len == 0)
            return 0.0f;
        else
            return heartRateDataPoints[len-1];
    }

    //calculates the 'frequency' of heart rate jumps in a set of data.
    //timeperiod is the time since this was last called.
    //numnewpoints is 'how many new points are we seeing since last time?'
    //we should really only be calculating over the new points.
    //but we can use the global set to calculate the running min, running max.
    //returns the heart rate.
    public float UpdateStats(Queue datapoints, int numNewPoints, float timeperiod, float hrSampleTime)
    {
        //Debug.Log("Update stats added time: " + timeperiod);
        //Update total time
        totalTime += timeperiod;

        //Update running min/max
        runningMin = 2;
        runningMax = -1;
        int index = 0;
        int count = datapoints.Count;
        
        foreach (float val in datapoints)
        {
            if (val > runningMax)
                runningMax = val;
            if (val < runningMin)
                runningMin = val;

            //If you're in the last "numNewPoints" section
            if ( (count - index - 1) < numNewPoints)
            {
                if (val - _prevBeatLevel > higherPulseThreshold)
                {
                    //that's a beat!
                    _numBeats += 1;
                    _beatTimes.Add(totalTime);
                    //Debug.Log("Beat at " + totalTime);
                }
            }
            index += 1;
            _prevBeatLevel = val;
        }


        // Calculate heart rate here.
        // For now, lets just use the last X seconds worth of data
        
        float heartRate = GetHeartRateOverTime(hrSampleTime);        
        heartRateDataPoints.Add(heartRate);
        
        //calculate new running avg
        _runningAvg = (_runningAvg * (heartRateDataPoints.Count-1) + heartRate ) /heartRateDataPoints.Count;

        if (heartRate > _highestHeartRate)
        {
            _highestHeartRate = heartRate;
        }
        if (heartRate > 0.0f && heartRate < _lowestHeartRate)
        {
            _lowestHeartRate = heartRate;
        }
        return heartRate;
    }

    public float HowHot()
    {
        //Debug.Log(_highestHeartRate);
        //Debug.Log(_runningAvg);
        
        //if (_highestHeartRate == 0.0f)
        //    return 0.0f;
        //float norm = _highestHeartRate - _runningAvg;
        float now = GetHeartRate() - _runningAvg;
        float high = _highestHeartRate - _runningAvg;
        
        //if (now < norm)
        //    return 0.0f;

        //float thresh = 0.5f;
        //Debug.Log("nownorm " + (now / norm));
        //if (now < norm * thresh) //20% but on a half scale
        //{
        //    return (1.0f - (now / (norm*thresh)) );
        //}
        if (now > 0.5 * high)
            return now / high;
        else
            return 0.0f;

    }

    public float GetTimeSinceLastHeartBeat()
    {
        if (_beatTimes.Count == 0)
            return 0.0f;
        return totalTime - _beatTimes[_beatTimes.Count-1];
    }

    public float GetHeartRateOverTime(float sampleTime)
    {   
        float heartRate = 0.0f;
        int beatindex = _beatTimes.Count-1;
        if (totalTime > sampleTime && beatindex >= 0)
        {
            float accumulator = 0.0f;
            
            float timeremaining = sampleTime;
            float t = _beatTimes[beatindex];
            while (beatindex >= 0 && (t - _beatTimes[beatindex] < sampleTime) )
            {
                accumulator += 1;
                //t -= _beatTimes[beatindex]; //difference between this and prev point.
                //timeremaining -= t;
                beatindex -= 1;
            }
            //we have 'accumulator' number of beats in 'sampleTime' seconds
            heartRate = accumulator * (60.0f / sampleTime);
        }
        return heartRate;
    }

    public float GetAverageHeartRate()
    {
        if (heartRateDataPoints.Count == 0)
            return 0;
        float accumulator = 0;
        foreach (float val in heartRateDataPoints)
        {
            accumulator += val;
        }
        return accumulator / heartRateDataPoints.Count;
    }

}
