﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HeartbeatPlugin : MonoBehaviour 
{
    //public ClientSideManager manager;
    public Text logText;
    public Text hrText;
    public Text minMaxText;
    static float RValue;
    string log;
    private bool dummy = false;

    AndroidJavaClass javaClass;
    AndroidJavaObject currentActivity;

	// Use this for initialization
	void Start () 
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }

	    try
        {
            AndroidJNI.AttachCurrentThread();
            using (javaClass = new AndroidJavaClass("com.pixeltron.heartbeat.MainActivity"))
            {
                if (javaClass != null)
                    currentActivity = javaClass.GetStatic<AndroidJavaObject>("mContext");
            }
        }
        catch
        {
            dummy = true;
        }

	}
	
	// Update is called once per frame
	void Update() 
    {
        if (currentActivity != null)
        {
            RValue = currentActivity.CallStatic<float>("getRate");
            log = currentActivity.CallStatic<string>("getLog");
        }
        else if (dummy == true)
        {
            RValue = Random.value * 40.0f;
        }
    
        ClientSideManager.instance.OnValue(RValue);
	}
}
