﻿using UnityEngine;
using System.Collections;
using LiteNetLib;
using LiteNetLib.Utils;


public class HeartBeatListener : MonoBehaviour, INetEventListener
{

    private NetClient _netClient;
    private NetPeer _ourPeer;
    private NetDataWriter _dataWriter;
    private ClientSideManager _csm;
    private float _sendData;
    private bool _newData;

//    private float _receivedFloat;
//    private float _lerpTime;
//    public Grapher grapher;

	// Use this for initialization
	void Start () 
    {
	    _netClient = new NetClient(this, "HeartBeat");
	    _netClient.Start();
	    _netClient.UpdateTime = 15;
        _dataWriter = new NetDataWriter();
        _csm = ClientSideManager.instance;
        _csm.Log("Client starting");
        _newData = false;
	}
	
	// Update is called once per frame
	void Update () 
    {
        //if (_netClient == null)
        //    return;
	    _netClient.PollEvents();

        if (_netClient.IsConnected)
        {
            //Fixed delta set to 0.05
            //var pos = _clientBallInterpolated.transform.position;
            //pos.x = Mathf.Lerp(_oldBallPosX, _newBallPosX, _lerpTime);
            //_clientBallInterpolated.transform.position = pos;
            //Debug.Log("Float:" + _receivedFloat );
            //grapher.QueueData(_receivedFloat);
            //Basic lerp
            //_lerpTime += Time.deltaTime/Time.fixedDeltaTime;
            
            if (_ourPeer != null && _newData == true)
            {
                _dataWriter.Reset();
                //Send it
                //_csm.Log("should send " + _sendData);
                _dataWriter.Put(_sendData);
                //_dataWriter.Put(_serverBall.transform.position.x);
                _ourPeer.Send(_dataWriter, SendOptions.Sequenced);
                _newData = false;
            }

        }
        else
        {
            _netClient.SendDiscoveryRequest(new byte[] { 1 }, 5000);
        }
	}
    public void SendData(float d)
    {
        _sendData = d;
        _newData = true;
    }

    public void OnNetworkReceive(NetPeer peer, NetDataReader reader)
    {
    }

    void OnDestroy()
    {
        if(_netClient != null)
            _netClient.Stop();
    }

    public void OnPeerConnected(NetPeer peer)
    {
        _csm.Log("[CLIENT] We connected to " + peer.EndPoint);
        _ourPeer = peer;
    }

    public void OnPeerDisconnected(NetPeer peer, DisconnectReason reason, int socketErrorCode)
    {
        _csm.Log("[CLIENT] We disconnected because " + reason);
        if (peer == _ourPeer)
            _ourPeer = null;
    }

    public void OnNetworkError(NetEndPoint endPoint, int socketErrorCode)
    {
        _csm.Log("[CLIENT] We received error " + socketErrorCode);
    }

    public void OnNetworkReceiveUnconnected(NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType)
    {
        if (messageType == UnconnectedMessageType.DiscoveryResponse && _netClient.Peer == null)
        {
            _csm.Log("[CLIENT] Received discovery response. Connecting to: " + remoteEndPoint);
            _netClient.Connect(remoteEndPoint);
        }
    }

    public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
    {
        
    }


}
