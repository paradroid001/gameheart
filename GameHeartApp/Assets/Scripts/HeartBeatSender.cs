﻿using UnityEngine;
using System.Collections;
using LiteNetLib;
using LiteNetLib.Utils;


public class HeartBeatSender : MonoBehaviour, INetEventListener
{

    private NetServer _netServer;
    private NetPeer _ourPeer;

    private float _receivedFloat;
    private float _lerpTime;
    public Grapher grapher;


	// Use this for initialization
	void Start () 
    {
        _netServer = new NetServer(this, 100, "HeartBeat");
        _netServer.Start(5000);
        _netServer.DiscoveryEnabled = true;
        _netServer.UpdateTime = 15;
        Debug.Log("Server listening");
	}
	
	// Update is called once per frame
	void Update () 
    {
	    _netServer.PollEvents();
	}

    void FixedUpdate()
    {
        if (_ourPeer != null)
        {
            //Debug.Log("Float:" + _receivedFloat );
            grapher.QueueData(_receivedFloat);
        }
    }

    public void OnNetworkReceive(NetPeer peer, NetDataReader reader)
    {
        _receivedFloat = reader.GetFloat();
        _lerpTime = 0f;
    }

    void OnDestroy()
    {
        if(_netServer != null)
            _netServer.Stop();
    }

    public void OnPeerConnected(NetPeer peer)
    {
        Debug.Log("[SERVER] We have new peer " + peer.EndPoint);
        _ourPeer = peer;
    }

    public void OnPeerDisconnected(NetPeer peer, DisconnectReason reason, int socketErrorCode)
    {
        Debug.Log("[SERVER] peer disconnected " + peer.EndPoint + ", info: " + reason);
        if (peer == _ourPeer)
            _ourPeer = null;
    }

    public void OnNetworkError(NetEndPoint endPoint, int socketErrorCode)
    {
        Debug.Log("[SERVER] error " + socketErrorCode);
    }

    public void OnNetworkReceiveUnconnected(NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType)
    {
        if (messageType == UnconnectedMessageType.DiscoveryRequest)
        {
            Debug.Log("[SERVER] Received discovery request. Send discovery response");
            _netServer.SendDiscoveryResponse(new byte[] {1}, remoteEndPoint);
        }
    }

    public void OnNetworkLatencyUpdate(NetPeer peer, int latency)
    {
        
    }


}
