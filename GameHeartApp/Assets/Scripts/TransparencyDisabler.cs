﻿using UnityEngine;
using System.Collections;

public class TransparencyDisabler : MonoBehaviour 
{
    public TransparentWindow transparentWindow;

    void Awake()
    {
        if (Application.platform != RuntimePlatform.WindowsPlayer)
        {
            transparentWindow.enabled = false;
        }
        else
        {
            transparentWindow.enabled = true;
        }
    }
}
