﻿using UnityEngine;
using System.Collections;
using System; //for exception
using UnityEngine.UI;

//Expects a canvas with a linerenderer object on it in the heirarchy
public class Grapher : MonoBehaviour 
{

    //public float graphWidth;
    //public float graphHeight;
    public AudioSource warningSound;
    public Image fire;
    public int numDataPoints;
    public LineRenderer heartRateLine;
    public LineRenderer heartBeatLine;
    public Text heartRateTextShortTerm;
    public Text heartRateTextMediumTerm;
    public Text heartRateTextLongTerm;
    public bool saveDataOnQuit;

    private float cameraSize; //"half" the camera's size
    private Queue _heartBeatValues; //for heart beat (pulse)
    private Queue _heartRateValues; //for heart rate (freq of pulse)
    //private LineRenderer lineRenderer;
    private HeartbeatStats heartbeatStats;
    private float _currentHeartRate;
    private Vector2 xybaseposHeartRate;
    private Vector2 xybaseposHeartBeat;
    private float _prevTime;
    private float _totalPointsReceived;

	// Use this for initialization
	void Start () 
    {
        cameraSize = Camera.main.orthographicSize;
        heartbeatStats = new HeartbeatStats();
	    InitControls();
        _heartBeatValues = new Queue(numDataPoints);
        _heartRateValues = new Queue(numDataPoints);

        xybaseposHeartBeat = new Vector2(0.0f, 0.2f);
        xybaseposHeartRate = new Vector2(0.8f, 0.2f);

        //Start with a sin wave pattern to test.
        for (int i = 0; i < numDataPoints; i++)
        {
            _heartBeatValues.Enqueue(Mathf.Sin(i) * 2); //multiplying for vertical scale?
            _heartRateValues.Enqueue(Mathf.Sin(i) * 2); //multiplying for vertical scale?
        }
        _prevTime = Time.time;
        _totalPointsReceived = 0;

	}

    void OnApplicationQuit()
    {
        if (saveDataOnQuit)
            heartbeatStats.Save();
    }
	
    void InitControls()
    {
        //graphWidth = heartBeatLine.gameObject.GetComponent<RectTransform>().rect.width;
        //graphHeight = heartBeatLine.gameObject.GetComponent<RectTransform>().rect.height;
        //Debug.Log("graphWidth =" + graphWidth + ", graphHeight =" + graphHeight);
        
        heartRateLine.sortingOrder = 1;
        heartRateLine.sortingLayerName = "UI";
        heartBeatLine.sortingOrder = 1;
        heartRateLine.sortingLayerName = "UI";

        heartRateLine.SetVertexCount(numDataPoints);
        heartBeatLine.SetVertexCount(numDataPoints);
    }

	// Update is called once per frame
	void Update () 
    {
        try
        {
	        DrawData();
        }
        catch (Exception e)
        {
            Debug.Log("Exception: " + e.ToString());
        }
	}

    public void QueueData(float num)
    {
        _totalPointsReceived += 1;
        float now = Time.time;
        _heartBeatValues.Enqueue(num);
        //
        //If an exception occurs in this line, bad things happen because
        //the queues don't get dequeued if they're overfull
        float b = heartbeatStats.UpdateStats(_heartBeatValues, 1, now-_prevTime, 8.0f);
        _heartRateValues.Enqueue(b);
        if (heartRateTextShortTerm != null)
            heartRateTextShortTerm.text = "" + (int)b;
        if (heartRateTextMediumTerm != null)
            heartRateTextMediumTerm.text = "" + (int)heartbeatStats.GetHeartRateOverTime(20.0f);
        if (heartRateTextLongTerm != null)
            heartRateTextLongTerm.text = "" + (int)heartbeatStats.GetHeartRateOverTime(180.0f);

        //"Scroll" data for heartbeat and heartrate
        if (_heartBeatValues.Count > numDataPoints)
        {
            _heartBeatValues.Dequeue();
        }
        if (_heartRateValues.Count > numDataPoints)
        {
            _heartRateValues.Dequeue();
        }
        //Debug.Log("Points: " + _totalPointsReceived);

        //Debug.Log("Heartrate: " + b + " at " + now);
        
        //If we've received more than a few heartbeats, get the time since the last one.
        if (warningSound != null && _heartBeatValues.Count > 10)
        {
            float timetolast = heartbeatStats.GetTimeSinceLastHeartBeat();
            float hr = heartbeatStats.GetHeartRate();

            if ( timetolast > (3.0f * 1.0f/(hr/60.0f) ))
            {
                if (!warningSound.isPlaying)
                    warningSound.Play();
            }
        }
        if (_heartRateValues.Count > 20 && fire != null)
        {
            Color c = fire.color;
            c.a = heartbeatStats.HowHot();
            fire.color = c;
        }

        _prevTime = now;
    }

    void DrawData()
    {
        //HeartBeat
        float ynormaliser = (float)Mathf.Abs(heartbeatStats.runningMax - heartbeatStats.runningMin);
        if (ynormaliser == 0.0f)
            ynormaliser = 1.0f;
        SetLinePoints(_heartBeatValues, heartBeatLine, new Vector2(2.0f * cameraSize / (float)numDataPoints, 1.0f / ynormaliser), xybaseposHeartBeat );

        //HeartRate
        float hrnormaliser = 0.8f;
        float lastHR = heartbeatStats.GetHeartRate();
        if (lastHR > 0)
        {
            hrnormaliser = 0.8f/lastHR;
        }
        SetLinePoints(_heartRateValues, heartRateLine, new Vector2(2*cameraSize / numDataPoints, hrnormaliser), xybaseposHeartRate );
        /*
        if (_heartBeatValues.Count > 0)
        {
            int i = 0;
            foreach (float item in _values)
            {
                int index = _heartBeatValues.Count - i - 1;
                Vector3 p = heartBeatLine.gameObject.transform.position;
                heartBeatLine.SetPosition(i, new Vector3( (5 * ( (i * _xInterval) / graphWidth) ) - 3f, 
                                                          1* ( item ) * Mathf.Abs((heartbeatStats.runningMax-heartbeatStats.runningMin)),
                                                          0.0f) );
                i++;
            }
        }
        */

        //HeartRate
        
    }

    void SetLinePoints(Queue data, LineRenderer line, Vector2 xyscale, Vector2 xybasepos)
    {
        int count = data.Count;
        if (count > numDataPoints)
            Debug.Log(data.Count);
        if (count > 0)
        {
            int i = 0;
            foreach (float item in data)
            {
                //int index = count - i - 1;
                //Vector3 p = line.gameObject.transform.position;
                float baseposy = ( 2 * cameraSize * xybasepos.x) - cameraSize;
                float screenscaley = (2 * cameraSize * xybasepos.y); //assuming values are between 0 and 1, taking up one 'unit' of camerasize by default
                Vector3 newLinePos = new Vector3((xyscale.x * i) - cameraSize, baseposy + (xyscale.y * item * screenscaley), 0.0f);
                //Debug.Log(newLinePos);
                line.SetPosition(i, newLinePos );
                i++;
            }
        }
    }
}
