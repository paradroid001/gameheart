﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ClientSideManager : MonoBehaviour 
{
    private static ClientSideManager _instance;

    public Button connectButton;
    public Text addressText;
    public Text portText;
    public Text logText;
    public Grapher grapher;
    public HeartBeatListener sender;

    public static ClientSideManager instance
    {
        get
        {
            //if this instance hasn't been set yet, we grab it from the scene.
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<ClientSideManager>();
                //Tell unity not to destroy this object when loading a new scene!
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }


	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void OnConnectClicked()
    {
        //sender.StartClient(addressText.text, int.Parse(portText.text) );
    }

    public void Log(string logtext)
    {
        logText.text += "\n" + logtext;
    }

    public void OnValue(float val)
    {
        grapher.QueueData(val);
        sender.SendData(val);
    }
}
